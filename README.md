# FOSS Online Instruction Toolkit (FOIT)

The purpose of this repository and project is to provide a single resource for
the creation of online communities with a particular focus on learning. This was
initially created in response to the pandemic that, without substantial warning,
closed the physical college and primary education spaces.

This project centers on a cultural, moral, and ethical foundation that such
spaces should be independent to capital and academic administration unless
absolutely necessary. With this in mind, only open-source tools that can be
completely self-hosted are used.

The term self-hosting refers to the physical space the software inhabits. While
hardware remains difficult to host one's self, and is outside of the scope of
this project but not absolutely outside anybody's means, the self-hosting of
software merely relates to the choice of which software to use. Each component
primarily functions independent of software on the wider network. Each person
making use of this infrastructure, once installed, can do so without talking to
a corporation or organization outside of your local network. (However, snooping
local networks and embeddable widgets, like whispering gossips, may still leak
information.)

Another aspect considered in curating the software for our purpose is an
emphasis on federation. This term refers to the nature in which this software
talks to software elsewhere. It may seem to contradict the ethical basis of
self-hosting stated above, yet there is merit in creating wider communities that
are bound to each other within that ethical scope. You can moderate which
communities you wish to collaborate with, and having decided this, communicate
with other instantiations (instances) without intrusion or technical burden from
any individuals within either collective.

This reduces, slightly, the absolute isolation of one server while maintaining
a strong sense of isolation of that, now, commune of servers.
That is, federation and self-hosting combined allow each of our islands to talk to each
other when appropriate while maintaining a moat around all of us.

## Components

We are making use of so much of the collective labor of our community. Here are
the players, from messaging servers to collaborative text editors.

**Matrix** is a *federated messaging platform* that is inspired by IRC, much like
Slack and Discord have been. There are two servers that are in play, here. One
server sits and listens for messaging traffic that is part of your local system.
The other server can communicate to the outside world, bringing conversation,
optionally, from other groups to members of your own community.

**Riot** is a *front-end for Matrix*. While Matrix just sits there and listens, it
does its simple job without a whole lot of pomp or posh. Riot resembles similar
proprietary systems such as Slack and Discord while adding UI for the very cool
federation and intergrated widgets as a web service. This is what you generally
interact with when using a Matrix system, here.

**TURN** is a *VoIP messaging server* which gives the Riot/Matrix system a means
of facilitating audio conferencing. It is how we speak to one another, no matter
how far apart we are. It implements the open TURN protocol. It is as simple as
that. (It is not... but it makes it look simple.)

**Etherpad** is a *collaborate text editing platform* that allows multiple people
to edit a text document simultaneously. We spin this up as a dedicated server
that you can elect to use on its own, however it is powerful when used as a
widget within a Matrix/Riot environment. It can "integrate" within the chat as
an embedded window allowing members of the chat channel the ability to watch
or even contribute.

**Dimension** is the *integration server* that allows Riot to talk to a wide
variety of other services we include in this toolkit. It tells Riot what services
exist and allows Riot to integrate services that are not a part of your self-hosted
environment, for instance YouTube or even an IRC/Slack bridge. It is a bit of the
odd-one out in this list, but an important bit of glue that holds it all together.

**PeerTube** is a *federated, peer-to-peer video hosting platform* that allows
videos to be hosted in a similar manner as YouTube. The interesting piece of flair,
here, is that this is federated in terms of discovery across other instances of
PeerTube, but also in terms of having peer-to-peer distribution. Ok, that's a lot
of words, but it means people watching the same video stream the bits that they
have already downloaded to each other as needed. This reduces the burden on any
one particular server to host the entire bandwidth... which is very expensive. It
also reduces the burden if pockets of students are geographically distant from
your server. People are global!

**PostgreSQL** is a *robust relational database*, and while it may be hidden away
in the background, it does a lot of heavy lifting across many of the above
services. Many of these other servers are making use of the same database server
and satisfying query after query. This database technology is open, secure, and
widely-used.

**Jitsi** is a *video conferencing service* that we can self-host on our server
to provide some form of video streaming. Like PeerTube, this uses some
sophisticated techniques to reduce the bandwidth burden across folks who are
watching. The service itself is complex. There are many, many pieces with very
strange names like Jigasi, Jicofo, and JVB... and I have no idea what they mean
either. As a whole, this is an impressive set of tools we can also embed into
our Riot/Matrix chat rooms.

## Other Tools

We will be leveraging a Linux environment of some kind along side Docker
containerization. A container is a useful concept that allows us to isolate
different software to make it easier to configure and use alongside the variety
of software in our list. This makes each program believe it is the only thing
installed on the system.

Doing so makes it much easier to pick and choose which services we want to
provide without having to worry about how they may interact. In fact, we can
isolate these services so they can only talk to each other when necessary which
also helps improve our security.

When we set up our system, we only need this infrastructure to be available in
order to set up everything else: A Linux server with Docker and Docker-Compose.
And, yes, these are also free of charge and open-source.

## Get a server

You will need a server that can be accessed by the world-at-large. You can
use your own, but a little extra work is necessary to get around a dynamic IP
address. This assumes you have a server with a known IP address.

For this, I recommend a dedicated server from
[OVH](https://www.ovh.com/world/dedicated-servers/) or
[Kimsufi](http://kimsufi.com/).
The cheapest will do, particularly for smaller classes.

The real requirement is network bandwidth.
In my quick testing, scaling to classrooms of 50 or more students generally
requires around 200 Mbps network, but likely make due with less.
OVH starts at a 500 Mbps connection, and Kimsufi maxes out at 100 Mbps.

Let's assume you are using Kimsufi (OVH is similar.) I purchase a dedicated
server. It takes a little bit for them to set it up for you, and you will
receive an email that it is up. You log on to your "Control Panel." and find
the machine listed there.

Right now, there's nothing on it! Exciting! So, we will install an OS.
You can install any Linux-based OS you want. You
will then need to install `git` and `docker-compose` using the package manager
on the OS, and then pull down this repository. Don't worry, we will go through
the steps here.

First step, as OVH helpfully tells you, let's install an OS.

![The OVH Install button](docs/images/readme_ovh_install.png)

We will select Ubuntu (alternatively, you can elect to install something else,
but I will be assuming this selection of Ubuntu 18.04.)

![The OVH Install dialog, with Ubuntu 18.04 selected from the drop-down](docs/images/readme_ovh_install_step1.png)

It gives you this scary dialog, but there's nothing on the machine. So we can
just press "Confirm" to move on.

![The OVH Install dialog, which says everything will be erased. Confirm button is at the bottom right.](docs/images/readme_ovh_install_step2.png)

Sit back and relax while it installs the OS for you. No need to configure things
or get bogged down in the details. Another reason why OVH/Kimsufi are good server
hosting platforms. On your dashboard it will display the progress: Make a note of
the "Main IP" as this will be important later.

![The OVH dashboard, showing the installation progress of the server.](docs/images/readme_ovh_install_step3.png)

While you are waiting, you can go grab a domain name!

## Get a domain name

While there have been strides to federate the naming system of the internet, it
just has not caught on just yet. The internet still requires a means of mapping
an arbitrary name to a particular computer, which itself is known via an
Internet Protocol (IP) address, such as `54.39.105.75`.

We need to spend a little bit of money to get our own domain name since we
require a set of subdomains. For this I recommend
[Namecheap](https://namecheap.com).

Once you purchase a domain name on namecheap (you can also transfer your existing
domain to namecheap, or do a similar process as we will see below on other hosting
providers,) you will navigate to the "Domains List" so we can set it up. This is
found off of your "Dashboard."

![The "Domains List" tab on your dashboard](docs/images/readme_namecheap_domains.png)

Here, you can find your domain in the list. And we will to find and click on that
"Manage" button.

![One of the domains listed we care about. To the right is a button named "Manage."](docs/images/readme_namecheap_domain.png)

From here, we want to navigate to the "Advanced DNS" tab, but do not worry, it is
not too complex.

![The tabs at the top. The right-most one says "Advanced DNS" which is selected.](docs/images/readme_namecheap_domain_header.png)

Now we need to go to our hosting provider and get the IP address of our machine.
The point of DNS is to map a name (such as wilkie.wtf, gitlab.com, etc) to a
particular computer. If you recall from our OVH dashboard we saw the "Main IP":

![The OVH dashboard showing the IP of our machine](docs/images/readme_namecheap_domain_ip.png)

Alright. Now we need to map all of our domain names to that machine, including
the special `@` name which maps the domain name itself. That record probably
points to a landing page by default, which you can modify and remove.

As mentioned and seen in the final screenshot below, will also add here all of
the nice "subdomains" which roll off of our normal name. This lets us isolate
each service we will be using into their own private spaces. A little more work
on our part, but worth the effort.

Use the "Add New Record" button until you have six rows like this, and modify
your table to look just like this one, except pasting in **your** IP address
instead of mine.

![The namecheap DNS table ](docs/images/readme_namecheap_domain_dns.png)

Press the "Save All Changes" button, and you are good to go. It may take a
little time for these records to properly propagate. The Internet is a big
place, and gossip is slow. Thankfully, we have some work to do in setting
up our server.

## Log on to your server.

Log on to your server.

If you are using an OVH/Kimsufi server, then you will have received an
email with your login credentials (and your IP address.) You can log on
to the machine with that username and password.

For Ubuntu, it has you log on as "root" which is a super-administrator
and not ideal. So the first step is to make a user just for yourself.
You'll need an SSH client, which the email will recommend and give you
a guide to use.

If we use PuTTY, as they recommend, we can log in by specifying the
username and domain much like an email address. If you have set up
your DNS correctly, we can use our domain!

![PuTTY interface showing me using the host root@wilkie.wtf](docs/images/readme_putty_step1.png)

Press "Open" and it should prompt you for the password. Since it is
long and complex, just copy and paste it in. Make sure you don't
include any spaces or junk when you are selecting it from the email.

After you log on, you will get a prompt. First, we want to update
the packages.

```shell
root@ns508346:~# apt update
```

Then we install the software we need to use. Say yes ("Y") every
time it asks you. Do not be too worried at this point; you can always reinstall
the entire OS with a button press if you feel things have gone all wrong.

```shell
root@ns508346:~# apt get docker-compose git
```

We will make ourselves an account. Use your own name and not mine, unless you
really like my name, which is fair. Mind the capitalization!! That is a
capital `G` and a lowercase `m`.

```shell
root@ns508346:~# useradd -m wilkie -G docker,sudo
```

Set a password for your account. Make it good. Everybody in the world can easily
attempt to log in. Use your username. (If you ever forget your password, you can
log on again as root with the password from the email and use this command again
to reset it!)

```shell
root@ns508346:~# passwd wilkie
```

And, finally, we will set a shell to make things more consistent in the rest
of the guide. We will use the `bash` shell instead of the default one.

```shell
root@ns508346:~# chsh -s /bin/bash wilkie
```

Close PuTTY and restart it, this time connecting as `wilkie@wilkie.wtf` where
you replace the username on the left with your own and, of course, the same
domain name you set up on the right. It will ask for your password and you
will enter the one you should created.

You are good to go! Now to setting up your software stack.
I know, I know... only now are you getting to this repository... but trust me:
the parts that happen next are easily the worst parts if not for the helpful
stuff in this project.

## Setting up your server

Once you are logged on to your server and have `git` and `docker-compose`
installed (as done above,) you can pull down this project with this command:
(This time DON'T replace my name with your own!) `:)`

```shell
wilkie@ns508346:~$ git clone https://gitlab.com/wilkie/foit
```

Navigate to the foit directory using the `cd` (Change Directory) command.

```shell
wilkie@ns508346:~$ cd foit
```

And now we are ready to configure our server.

## Configuration

Now comes the fun part, making your instance unique.

The following script will set up the basic configuration and allow you to
alter it later. It will also set up administrator accounts which can be used
to configure many aspects of the service when you log in as that account.

```shell
wilkie@ns508346:~/foit$ ./scripts/configure
```

Just follow the directions! The defaults will give you absolutely everything.

When it is done, and things have successfully spun up, you can navigate to
your domain. I would start with your chat subdomain. Mine would be, in this
example, `chat.wilkie.wtf`, so just point your browser to that, and away you go!

## Shut down your server software

When you want to turn off your server you can run the command:

```shell
wilkie@ns508346:~/foit$ ./scripts/down
```

## Turn on your server software

When you want to spin up your server, run this command from the `foit`
directory:

```shell
wilkie@ns508346:~/foit$ ./scripts/up
```

## Adding Riot/Matrix Users

The configure script already sets up an administrator account with the username
and password you specified.

To add other chat accounts (Riot/Matrix) at any point use the following command:

```shell
./scripts/add-account mydomain.com
```

It will prompt you as such, where you can fill in the information.
You may choose a unique name for each account.
You generally want yourself to be an administrator (but nobody else!) so say 'yes' to that option the first time.

**Note**: It will not show the password while you are typing it in. The options within brackets are the default option. Leaving your answer blank will be considered the same as the content within the brackets.

```
Name: wilkie
Password: 
Confirm password:
Make admin [no]: no
Sending registration request...
Success!
```

## Adding Peertube Users

The peertube server is created with a root account by default with a password
supplied to you when you configured your setup. The root password is also
contained in the `.env` file in the `domains/mydomain.com` directory, where
`mydomain.com` is the domain you specified and setup.

Normally, you likely
do not want others to post videos, but you might want to do so at some point.
You can use your root account to create an account just for you that serves as
an administrator account and your personal account. From there you can create
lesser-privileged accounts for others, if you want. People do not need an
account to view videos, by default, unless you make them private. You can
create videos unlisted, as well, to avoid some issues, if you want, and then
post them in your authenticated chat server!

Log on to your video server (likely at a `video` subdomain) and navigate to the
users page. Add an account using the webpage. Non-administrator accounts cannot
create users, so that's a good default role.
