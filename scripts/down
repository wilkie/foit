#!/bin/bash
set -eu

USAGE="USAGE: ./scripts/down [example.com] [service]"

SHARED=foit-shared
DOMAIN=
if [ ${#@} -gt 0 ]; then
  DOMAIN=$1
fi
ARGS=${@:2}

if [ ! -z ${DOMAIN} ] && [ ! -e domains/${DOMAIN} ]; then
  echo ${USAGE}
  echo ""
  echo "You must specify an existing domain to stop."
  echo ""
  echo "Installed domains:"
  ls domains
  exit 1
fi

ROOT=$PWD/domains

# For each domain, up that domain from that directory
if [ -z ${DOMAIN} ]; then
  cd ${ROOT}/${SHARED}
  if [ -e SERVICES ]; then
    docker-compose `cat SERVICES` down --remove-orphans ${ARGS}
  fi

  for domain in `ls ${ROOT}`; do
    if [ "${domain}" != "${SHARED}" ]; then
      cd ${ROOT}/${domain}
      if [ -e SERVICES ]; then
        docker-compose `cat SERVICES` down --remove-orphans
      fi
    fi
  done
else
  cd ${ROOT}/${SHARED}
  if [ -e SERVICES ]; then
    docker-compose `cat SERVICES` down --remove-orphans ${ARGS}
  fi

  cd ${ROOT}/${DOMAIN}
  docker-compose `cat SERVICES` down --remove-orphans ${ARGS}
fi
