#!/bin/bash
set -eu

USAGE="USAGE: ./scripts/up [example.com] [service]"

SHARED=foit-shared
DOMAIN=
if [ ${#@} -gt 0 ]; then
  DOMAIN=$1
fi
ARGS=${@:2}

if [ ! -z ${DOMAIN} ] && [ ! -e domains/${DOMAIN} ]; then
  echo ${USAGE}
  echo ""
  echo "You must specify an existing domain to start."
  echo ""
  echo "Installed domains:"
  ls domains
  exit 1
fi

ROOT=$PWD/domains

# For each domain, up that domain from that directory
if [ -z ${DOMAIN} ]; then
  for domain in `ls domains`; do
    if [ "${domain}" != "${SHARED}" ]; then
      cd ${ROOT}/${domain}
      if [ -e SERVICES ]; then
        docker-compose `cat SERVICES` up -d
      fi
    fi
  done

  cd ${ROOT}/${SHARED}
  if [ -e SERVICES ]; then
    docker-compose `cat SERVICES` up -d
  fi
else
  cd ${ROOT}/${SHARED}
  if [ -e SERVICES ]; then
    docker-compose `cat SERVICES` up -d
  fi

  cd ${ROOT}/${DOMAIN}
  docker-compose `cat SERVICES` up -d ${ARGS}
fi
